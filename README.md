# Vertica Exporter

Test for Vertica vsql exporter

## 환경구성   

1. golang 설치   
```
wget https://dl.google.com/go/go1.13.9.linux-amd64.tar.gz   
tar -C /usr/local -xzf go1.11.5.linux-amd64.tar.gz   
export PATH=$PATH:/usr/local/go/bin   
source ~/.bash_profile 
export GOPATH="/root/nsheo"
```
2. make/fpm 설치   
```
yum install -y ruby-devel gcc make rpm-build rubygems   
gem install --no-document fpm   
```
3. vertica_exporter 다운로드
```
git clone https://gitlab.com/nsheo/vertica_exporter.git   
cd vertica_exporter_test   
<!-- 조회할 SQL 내용에 따라 편집가능 -->   
cp misc/settings_example.json settings.json   
vi misc/vertica_exporter.default   
<!-- DB접속정보 및 exporter listener port 변경 -->   
vi main.go

func run() (err error) {
        listen := flag.String("listen", ":9401", "listen on")
        dbLogin := flag.String("vertica_login", "dbadmin", "vertica username")
        dbPassword := flag.String("vertica_password", "ntels", "vertica password")
        dbHost := flag.String("vertica_host", "192.168.10.54:5433", "vertica host")
        pingPeriodPtr := flag.String("ping-period", "10s", "database ping period")
        timeoutPtr := flag.String("timeout", "5s", "database timeout")
        settingsFile := flag.String("settings", "./settings.json", "files with settings")
```

## 빌드 및 실행
1. 빌드 및 실행
```    
go get #의존성 패키지 다운로드   
go build #golang 컴파일러 실행   
./vertica_exporter_test #프로그램 실행   
```   

2. service 등록(centos 기준)
```
vi vertica_exporter.service   
   
[Unit]   
Description=Prometheus vertica exporter   
ConditionPathExists=/root/nsheo/src/vertica_exporter/vertica_exporter    
After=network.target   
   
[Service]   
Type=simple   
LimitNOFILE=1024   
Restart=on-failure   
RestartSec=10   
startLimitIntervalSec=60   
WorkingDirectory=/root/nsheo/src/vertica_exporter   
ExecStart=/root/nsheo/src/vertica_exporter/vertica_exporter --vertica_login=dbadmin --vertica_password=ntels --vertica_host="192.168.10.54:5433"  
PermissionsStartOnly=true   
ExecStartPre=/bin/mkdir -p /var/log/vertica_exporter   
ExecStartPre=/bin/chmod 755 /var/log/vertica_exporter   
StandardOutput=syslog   
StandardError=syslog   
SyslogIdentifier=vertica_exporter   
   
[Install]   
WantedBy=multi-user.target   

cp vertica_exporter.service /etc/systemd/system/vertica_exporter.service

systemctl enable vertica_exporter.service
```
